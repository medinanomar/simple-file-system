// fs.cpp: File System

#include "sfs/fs.h"

// #include <algorithm>

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

//Needed to compare unsigned numbers, classmate recommended this.
#define min(X,Y) (((X) < (Y)) ? (X) : (Y))
#define max(X,Y) (((X) > (Y)) ? (X) : (Y))


Disk *mounted_disk;
uint32_t n_blocks;
uint32_t block_num;
uint32_t inode_num;
uint32_t *bitmap;


bool save_inode(size_t inumber, Inode *node);
bool load_inode(size_t inumber, Inode *node);
size_t allocate_free_block();

void debug(Disk *disk) {
    Block block;

    disk->readDisk(disk, 0, block.Data);

    printf("SuperBlock:\n");
    printf("    magic number is %s\n", (block.Super.MagicNumber == MAGIC_NUMBER? "valid":"invalid"));
    printf("    %u blocks\n"         , block.Super.Blocks);
    printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
    printf("    %u inodes\n"         , block.Super.Inodes);

    int numberOfInodeBlocks = block.Super.InodeBlocks;
    if(!numberOfInodeBlocks) printf("why\n");
    for(int inodeBlock = 1; inodeBlock <= numberOfInodeBlocks; inodeBlock++) {
        disk->readDisk(disk, inodeBlock, block.Data);
        for (unsigned int currentInode = 0; currentInode < INODES_PER_BLOCK; currentInode++) {
            Inode inode = block.Inodes[currentInode];
            if (!inode.Valid)
                continue;
            printf("Inode %d:\n", currentInode);
            printf("    size: %u bytes\n", inode.Size);
            printf("    direct blocks:");
            for (unsigned int directBlock = 0; directBlock < POINTERS_PER_INODE; directBlock++) {
                unsigned int directBlockNumber = inode.Direct[directBlock];
                if (directBlockNumber == 0) continue;
                printf(" %u", directBlockNumber);
            }
            printf("\n");
            unsigned int indirectPointer = inode.Indirect;
            // we begin with first indirect
            if (indirectPointer != 0) {
                Block ind_block;
                printf("    indirect block: %u\n", indirectPointer);
                printf("    indirect data blocks:");
                disk->readDisk(disk, indirectPointer, ind_block.Data);
                for (unsigned int indirectBlock = 0; indirectBlock < POINTERS_PER_BLOCK; indirectBlock++) {
                    indirectPointer = ind_block.Pointers[indirectBlock];
                    if (indirectPointer == 0 || indirectPointer > POINTERS_PER_BLOCK) continue;
                    printf(" %u", indirectPointer);
                }
                printf("\n");
            }
        }
    }
}

bool format(Disk *disk) {
    //Check if disk is mounted
    if (disk->mounted(disk)) {
        return false;
    }
    // Write superblock
    Block block;
    block.Super.MagicNumber = MAGIC_NUMBER;
    block.Super.Blocks = disk->Blocks;
    unsigned int inodeblocks =  (unsigned int)(disk->Blocks*0.1 + 0.5);
    block.Super.InodeBlocks = inodeblocks;
    block.Super.Inodes = INODES_PER_BLOCK*block.Super.InodeBlocks;
    disk->writeDisk(disk, 0, block.Data);


    // Clear all other blocks
    char empty_block[BLOCK_SIZE] = {0};
    for(int inodeBlock = 1; inodeBlock < block.Super.Blocks; inodeBlock++){
        disk->writeDisk(disk,inodeBlock,empty_block);
    }

    return true;
}




bool mount(Disk *disk) {
    //Check if disk is mounted
    if (disk->mounted(disk)) {
        return false;
    }

    Block block;
    disk->readDisk(disk,0,block.Data);
    // Read superblock
    if (block.Super.Inodes != block.Super.InodeBlocks * INODES_PER_BLOCK || block.Super.InodeBlocks !=(unsigned int) (.1 * block.Super.Blocks +0.5) || block.Super.Blocks <0 || block.Super.MagicNumber != MAGIC_NUMBER)  {
        return false;
    }

    // Set device and mount
    disk->mount(disk);

    // Copy metadata
    n_blocks = block.Super.Blocks;
    block_num = block.Super.InodeBlocks;
    inode_num = block.Super.Inodes;
    mounted_disk = disk;
    // Allocate free block bitmap
    bitmap = (unsigned int*) calloc(n_blocks,sizeof(unsigned int));
    for(int i = 0; i < n_blocks; i++){
        bitmap[i] = 1;
        if(i == n_blocks-1) bitmap[i] = 0;
    }
    for(int i = 1; i < block_num; i++) bitmap[i] = 0;

    for(int inodeBlock = 1; inodeBlock <= block_num; inodeBlock++){
        Block inode_block;
        disk->readDisk(disk,inodeBlock,inode_block.Data);
        for(unsigned int currentInode = 0; currentInode < INODES_PER_BLOCK; currentInode++){
            Inode inode = block.Inodes[currentInode];
            if(inode.Valid == 0)
                continue;
            unsigned int num_blocks = (unsigned int)(inode.Size/(double)BLOCK_SIZE + 0.5);
            for(unsigned int directBlock = 0; directBlock < POINTERS_PER_INODE && directBlock < num_blocks; directBlock++){
                unsigned int directBlockNumber = inode.Direct[directBlock];
                if(directBlockNumber == 0)
                    continue;
                bitmap[directBlockNumber] = 0;
            }
            unsigned int indirectPointer = inode.Indirect;
            if(num_blocks > POINTERS_PER_INODE) {
                Block ind_block;
                disk->readDisk(disk, indirectPointer, ind_block.Data);
                bitmap[indirectPointer] = 0;
                for(unsigned int indirectBlock = 0; indirectBlock < num_blocks - POINTERS_PER_BLOCK; indirectBlock++){
                    indirectPointer = ind_block.Pointers[indirectBlock];
                    if(indirectPointer == 0 || indirectPointer > POINTERS_PER_BLOCK) continue;
                    bitmap[indirectPointer] = 0;
                }
            }
        }

    }
    return true;
}


size_t create() {
    // Locate free inode in inode table
    size_t inode_to_return = -1;
    if(!mounted_disk->mounted(mounted_disk))
        return -1;

    for(int inode_b = 1; inode_b <= block_num; inode_b++) {
        Block temp;
        mounted_disk->readDisk(mounted_disk,inode_b,temp.Data);
        for(unsigned int currentInode = 0; currentInode < INODES_PER_BLOCK; currentInode++){
            // Record inode if found
            Inode inode = temp.Inodes[currentInode];
            if(!inode.Valid){
                inode_to_return = currentInode + INODES_PER_BLOCK*(inode_b-1);
                break;
            }
        }
        if(inode_to_return != -1)
            break;
    }
    if(inode_to_return == -1) return -1;
    Inode inode;
    inode.Indirect = 0;
    inode.Size = 0;
    inode.Valid = 1;
    for (int i = 0; i < POINTERS_PER_INODE; i++) {
        inode.Direct[i] = 0;
    }
    save_inode(inode_to_return, &inode);
    return inode_to_return;
}


bool removeInode(size_t inumber) {
    //Check if disk is mounted
    if(!mounted_disk->mounted(mounted_disk)) {
        return false;
    }
    // Load inode information
    Inode inode;
    bool loaded = load_inode(inumber,&inode);
    if(!loaded || !inode.Valid) return false;
    // Free direct blocks
    for(int i = 0; i < POINTERS_PER_INODE;i++){
        unsigned int direct = inode.Direct[i];
        if(direct){
            bitmap[direct] = 1;
            inode.Direct[i] = 0;
        }
    }

    unsigned int indirectPointer = inode.Indirect;
    if(indirectPointer){
        // Load indirect block
        Block indirect_b;
        mounted_disk->readDisk(mounted_disk,indirectPointer,indirect_b.Data);
        for(int i = 0; i < POINTERS_PER_BLOCK; i++){
            unsigned int indirect_p = indirect_b.Pointers[i];
            // Free indirect blocks
            if(indirect_p){
                bitmap[indirect_p] = 1;
                indirect_b.Pointers[i] = 0;
            }
        }
    }

    // Clear inode in inode table
    inode.Valid = 0;
    inode.Indirect = 0;
    inode.Size = 0;
    for(int i = 0; i < POINTERS_PER_INODE; i++){
        inode.Direct[i] = 0;
    }

    if(!save_inode(inumber,&inode))
        return false;
    return true;
}



bool save_inode(size_t inumber, Inode *node){
    if (inumber >= inode_num) return false;
    Block block;
    mounted_disk->readDisk(mounted_disk,1 + inumber / INODES_PER_BLOCK,block.Data);
    block.Inodes[inumber % INODES_PER_BLOCK] = *node;
    mounted_disk->writeDisk(mounted_disk,1 + inumber / INODES_PER_BLOCK,block.Data);
    return true;
}


bool load_inode(size_t inumber, Inode *node){
    Block block;
    if (inumber >= inode_num) return false;
    mounted_disk->readDisk(mounted_disk,(1 + (inumber / INODES_PER_BLOCK)),block.Data);
    *node = block.Inodes[inumber % INODES_PER_BLOCK];
    return true;

}



size_t stat(size_t inumber) {
    // Load inode information
    Inode inode;
    if (!load_inode(inumber,&inode) || !inode.Valid) {
        return -1;
    }
    return inode.Size;
}


size_t readInode(size_t inumber, char *data, size_t length, size_t offset) {

    Inode inode;
    size_t read_index =0;
    // Load inode information and check if its there
    if(!load_inode(inumber,&inode))
        return -1;

    // Adjust length
    unsigned int offset_block = offset / BLOCK_SIZE;
    size_t len_read, diff_read;
    length = min(length, inode.Size - offset);


    Block indirect;
    if ((offset + length) / BLOCK_SIZE > POINTERS_PER_INODE) {
        if (inode.Indirect == 0) return -1;
        mounted_disk->readDisk(mounted_disk, inode.Indirect,indirect.Data);
    }

    size_t  next_read;
    for (unsigned int i = offset_block; read_index < length; i++) {
        if (i < POINTERS_PER_INODE) next_read = inode.Direct[i];
        else next_read = indirect.Pointers[i-POINTERS_PER_INODE];

        if (next_read == 0)  return -1;


        Block bl;
        mounted_disk->readDisk(mounted_disk,next_read,bl.Data);
        if (read_index == 0) {
            diff_read = offset % BLOCK_SIZE;
            len_read = min(BLOCK_SIZE - diff_read, length); }
        else {
            len_read = min(BLOCK_SIZE-0, length-read_index);
            diff_read = 0;
        }
        // Read block and copy to data
        memcpy(data + read_index, bl.Data + diff_read, len_read);
        read_index += len_read;
    }
    return read_index;

}

//Could not implement :(
size_t writeInode(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode

    return 0;
}